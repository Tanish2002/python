a=420
print("a is a int with value=", a)
a = float(a)
print("a is a float now with value=", a)
a = str(a)
print("a is a string now with value=", a)

for i in range(2):
    print()

b="hello"
print("b is a string with value=", b)
print("as b has letters as value it can't be casted into int or float but...")

for i in range(2):
    print()

c="69"
print("c is a string with value=", c)
print("and it can be casted into float and int as it has numbers as value")
c = int(c)
print("c is a int now with value=", c)
c = float(c)
print("c is a float now with value=", c)

for i in range(2):
    print()

print("Bonus!!! tip")
print("Booleans have only 2 values and casting them takes only numbers inside similar to casting string to int and float")
print("Also for any number other than 0 bool() gives only True and False if 0")
print(bool(0))
print(bool(1))
print(bool(213))
print(bool(-1533))
